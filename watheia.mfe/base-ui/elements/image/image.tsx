/** @format */

import React from "react"

import { staticStorageUrl } from "@watheia/mfe.base-ui.constants.storage"

import classNames from "classnames"
import styles from "./image.module.scss"

export type ImageProps = {
  /**
   * alt text (to comply a11y standards)
   */
  alt: string
  /**
   * Makes image fill the whole width of container.
   * same as `width: 100%`
   */
  fullWidth?: boolean
} & React.ImgHTMLAttributes<HTMLImageElement>

/**
 * Template component for images.
 * Accepts all the arguments of native html image.
 * @example
 * 	<BaseImage alt="avatar" src="https://storage.com/asdlkfjsdf.png">
 */
export function BaseImage({ alt, className, fullWidth, ...rest }: ImageProps) {
  return (
    <img
      data-bit-id="bit.base-ui/elements/image"
      alt={alt}
      {...rest}
      className={classNames(styles.image, fullWidth && styles.fullWidth, className)}
    />
  )
}

/**
 * Concrete image, using our Static Storage CDN.
 *
 * Treats src as relative paths on top of our image storage, and supports all other properties an html `<img>` would.
 * @name Image
 * @example
 * <Image src="homepage-bit/map.png" alt="illustration" fullWidth />
 */
export function Image({ src, ...rest }: ImageProps) {
  return (
    <BaseImage
      data-bit-id="bit.evangelist/elements/image"
      {...rest}
      src={`${staticStorageUrl}/${src}`}
    />
  )
}
