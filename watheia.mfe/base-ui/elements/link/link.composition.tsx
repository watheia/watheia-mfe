/** @format */

import React from "react"
import { ThemeCompositions } from "@teambit/documenter.theme.theme-compositions"
import { Link } from "./link"

const Center = ({ children }: React.HTMLAttributes<HTMLDivElement>) => {
  return (
    <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
      {children}
    </div>
  )
}

export const BaseLink = () => (
  <Center>
    <Link href="https://bit.dev">bit.dev</Link>
  </Center>
)

export const BaseLinkWithExternal = () => (
  <Center>
    <Link href="https://bit.dev" external={true} data-testid="test-link">
      bit.dev
    </Link>
  </Center>
)

export const LinkExample = () => (
  <ThemeCompositions>
    <Link href="https://google.com" data-testid="test-link">
      look it up!
    </Link>
  </ThemeCompositions>
)

export const LinkWithExternal = () => (
  <ThemeCompositions>
    <Link href="https://google.com" external data-testid="test-link">
      look it up!
    </Link>
  </ThemeCompositions>
)

const compositions = [LinkExample, LinkWithExternal]
// @ts-ignore
compositions.map((comp) => (comp.canvas = { height: 90 }))
