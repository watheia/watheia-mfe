/** @format */

import React from "react"
import classNames from "classnames"

export type LinkProps = {
  /**
   * opens link in a new tab
   */
  external?: boolean
} & React.AnchorHTMLAttributes<HTMLAnchorElement>

import styles from "./link.module.scss"

export function BaseLink(props: LinkProps) {
  const { external, children, ...rest } = props

  const externalProps = external ? { rel: "noopener", target: "_blank" } : {}

  return (
    <a data-bit-id="bit.base-ui/elements/link" {...externalProps} {...rest}>
      {children}
    </a>
  )
}

/**
 * A concrete link, styled for Evangelist, extending [base link](https://bit.dev/bit/base-ui/elements/link).
 * Accepts all props as html Anchor Element.
 *
 * The link will use these css variables, when available:
 * -   --primary-link-color, or --base-color, for text color.
 * -   --primary-link-highlight, or --base-highlight, for text color on hover.
 * @name Link
 * @example
 * <Link href="https://google.com">look it up!</Link>
 */
export function Link(props: LinkProps) {
  return (
    <BaseLink
      data-bit-id="bit.evangelist/elements/link"
      {...props}
      className={classNames(props.className, styles.primaryLink)}
    />
  )
}
