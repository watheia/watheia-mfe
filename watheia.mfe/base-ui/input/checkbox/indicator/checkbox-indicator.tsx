/** @format */

import React from "react"
import classnames from "classnames"
import { Icon } from "@watheia/mfe.base-ui.elements.icon"
import styles from "./checkbox-indicator.module.scss"

export type CheckboxIndicatorProps = React.HTMLAttributes<HTMLSpanElement>

export const classes = {
  checkedIndicator: styles.checkedIndicator,
  defaultCheckbox: styles.defaultCheckbox,
}

/**
 * 'Vanilla' base component for checkbox indicator. Mirrors preceding checkbox.
 */
export function CheckboxIndicatorBase(props: CheckboxIndicatorProps) {
  const { className = classes.defaultCheckbox } = props

  return <span {...props} className={classnames(className, classes.checkedIndicator)} />
}

/**
 * Mirrors a preceding checkbox input, with a checkmark icon.
 */
export function CheckboxIndicator(props: CheckboxIndicatorProps) {
  return (
    <CheckboxIndicatorBase
      {...props}
      className={classnames(props.className, styles.checkmarkInputIndicator)}
    >
      <Icon of="check-mark" className={styles.icon} />
    </CheckboxIndicatorBase>
  )
}
