/** @format */

import React, { isValidElement, ReactNode, ChangeEvent } from "react"
import styles from "./checkbox-label.module.scss"
import { CheckboxIndicator, classes } from "@watheia/mfe.base-ui.input.checkbox.indicator"

import { HiddenCheckbox } from "@watheia/mfe.base-ui.input.checkbox.hidden"

export { HiddenCheckbox as Input }

export interface CheckboxLabelProps extends React.LabelHTMLAttributes<HTMLLabelElement> {
  /**
   * Custom `input` element. Use this to pass any custom props to the input element.
   */
  input?: ReactNode
  /**
   * A visual facade to the `input` element.
   * Style it using the `input:<state> + indicator { }` to achieve a look not possible with the native `<input/>` element.
   */
  indicator?: ReactNode
  /**
   * Set the `checked` property of the default `input`. When using a custom `input`, pass props directly to it.
   */
  checked?: boolean
  /**
   * Set the `onChange` property of the default `input`. When using a custom `input`, pass props directly to it.
   */
  onInputChanged?: (event: ChangeEvent<HTMLInputElement>) => void
  /**
   * Set the `disabled` property of the default `input`. When using a custom `input`, pass props directly to it.
   */
  disabled?: boolean
}

/** Custom checkbox with text. */
export function CheckboxLabelBase({
  checked,
  defaultChecked,
  onInputChanged,
  disabled,
  input = (
    <HiddenCheckbox
      defaultChecked={defaultChecked}
      onChange={onInputChanged}
      checked={checked}
      disabled={disabled}
    />
  ),
  indicator = <CheckboxIndicator className={classes.defaultCheckbox} />,
  children,
  ...rest
}: CheckboxLabelProps) {
  return (
    <label {...rest}>
      {input}
      {indicator}
      {children}
    </label>
  )
}

/** A styled checkbox with text. */
export function CheckboxLabel(props: CheckboxLabelProps) {
  const child =
    isValidElement(props.children) || !!props.children ? (
      props.children
    ) : (
      <span>{props.children}</span>
    )

  return (
    <CheckboxLabelBase
      {...props}
      className={styles.checkboxLabel}
      indicator={<CheckboxIndicator />}
    >
      {child}
    </CheckboxLabelBase>
  )
}
