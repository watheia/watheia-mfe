import React from 'react';
import { render } from '@testing-library/react';
import { BasicText } from './text.compositions';

describe('Text', () => {
  it('should render the text in a span', () => {
    const { container } = render(<BasicText />);
    expect(container.querySelector('span').textContent).toEqual('Foo and bar');
  });
});
