/** @format */

import * as path from "path"
import yaml from "js-yaml"
import fse from "fs-extra"
import { map, compact, has, merge, reduce, set } from "lodash"

const metadataFileName = "metadata.json"

/**
 * Supported parsers
 */
export const parsers = {
  yaml: (data: any) => yaml.load(data, { schema: yaml.JSON_SCHEMA }),
  json: (data: any) => JSON.parse(data),
}

/**
 * A map of extension names to parser
 */
export const supportedExtensions = {
  yaml: parsers.yaml,
  yml: parsers.yaml,
  json: parsers.json,
}

/**
 *
 * Reads in a map of relative file locations from a base path and
 * returns a parsed representation of all data transomed into a
 * map of objects (json).
 */
export function slurpDataFiles({ dataFiles, dataDirPath, reporter }) {
  let promises = map(dataFiles, (filePath) => {
    const pathObject = path.parse(filePath)
    const absFilePath =
      pathObject.base === metadataFileName
        ? metadataFileName
        : path.join(dataDirPath, filePath)
    const relPath = pathObject.base === metadataFileName ? metadataFileName : filePath
    const relDir = pathObject.base === metadataFileName ? "" : pathObject.dir
    const ext = pathObject.ext.substring(1)
    if (!has(supportedExtensions, ext)) {
      return null
    }
    return fse.readFile(absFilePath).then((data) => {
      const propPath = compact(relDir.split(path.sep).concat(pathObject.name))
      const res = {}
      try {
        const parsedData = supportedExtensions[ext](data)
        set(res, propPath, parsedData)
      } catch (err) {
        reporter.warn(`[gatsby-source-data] could not parse file: ${relPath}`)
      }
      return res
    })
  })
  return Promise.all(promises).then((results) => {
    return reduce(results, (data, res) => merge(data, res), {})
  })
}

export default slurpDataFiles
