/** @format */

import { WithPreviewReactAspect, WithPreviewReactConfig } from "./mfe-nextjs-preview.aspect"

export type { WithPreviewReactMain } from "./mfe-nextjs-preview.main.runtime"
export type { WithPreviewReactPreview } from "./mfe-nextjs-preview.preview.runtime"
export { WithPreviewReactAspect, WithPreviewReactConfig }
export default WithPreviewReactAspect
