/** @format */

import React, { HTMLAttributes } from "react"
import classNames from "classnames"
import "reset-css"
import { Theme } from "@watheia/mfe.base-ui.theme.theme-provider"
import { Roboto } from "@watheia/mfe.base-ui.theme.fonts.roboto"
import { IconFont } from "@watheia/mfe.waweb.theme.icon-font"
import sizes from "./sizes.module.scss"
import global from "./global.module.scss" // TODO - rename
import colors from "./colors.module.scss"

export type ThemeContextProps = {} & HTMLAttributes<HTMLDivElement>

export function ThemeContext({ children, className, ...rest }: ThemeContextProps) {
  return (
    <Theme
      {...rest}
      className={classNames(className, sizes.heading, global.overrides, colors.status)}
    >
      <IconFont />
      <Roboto />
      {children}
    </Theme>
  )
}
